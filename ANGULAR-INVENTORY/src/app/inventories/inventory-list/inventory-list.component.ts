import { Component, OnInit } from '@angular/core';

import { InventoryService } from '../shared/inventory.service'
import { InventoryDetails } from '../shared/inventory.model';
import { ToastrService } from 'ngx-toastr';    
@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.css']
})
export class InventoryListComponent implements OnInit {

  constructor(public inventoryService: InventoryService,public toastr : ToastrService) { }

  ngOnInit() {
    this.inventoryService.getInventoryList();
  }

  showForEdit(emp: InventoryDetails) {
    this.inventoryService.selectedInventory = Object.assign({}, emp);;
  }


  onDelete(id: number) {
    if (confirm('Are you sure to delete this record ?') == true) {
      this.inventoryService.deleteInventory(id)
      .subscribe(x => {
        this.inventoryService.getInventoryList();
        this.toastr.warning("Deleted Successfully","Inventory Register");
      })
    }
  }
}
