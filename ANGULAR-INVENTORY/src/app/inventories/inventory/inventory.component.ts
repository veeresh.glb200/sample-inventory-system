import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms'


import { InventoryService } from '../shared/inventory.service'
import { ToastrService } from 'ngx-toastr'
@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {

  constructor(public inventoryService: InventoryService, public toastr: ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
    this.inventoryService.selectedInventory = {
      InventoryID: null,
      Name: '',
      Price: '',
      Quantity: 0,
      CreatedDate: ''
    }
  }

  onSubmit(form: NgForm) {
    if (form.value.InventoryID == null) {
      this.inventoryService.postInventory(form.value)
        .subscribe(data => {
          this.resetForm(form);
          this.inventoryService.getInventoryList();
          this.toastr.success('New Record Added Succcessfully', 'Inventory Register');
        })
    }
    else {
      this.inventoryService.putInventory(form.value.InventoryID, form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.inventoryService.getInventoryList();
        this.toastr.info('Record Updated Successfully!', 'Inventory Register');
      });
    }
  }
}
