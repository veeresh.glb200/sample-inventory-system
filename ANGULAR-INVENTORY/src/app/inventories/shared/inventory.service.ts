import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {InventoryDetails} from'./inventory.model'

@Injectable()
export  class InventoryService {

  selectedInventory : InventoryDetails;
  inventoryList : InventoryDetails[];
  constructor(private http : Http) { }

  postInventory(emp : InventoryDetails){
    var body = JSON.stringify(emp);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post,headers : headerOptions});
    return this.http.post('http://localhost:28750/api/Inventory',body,requestOptions).map(x => x.json());
  }

  putInventory(id, emp) {
    var body = JSON.stringify(emp);
    var headerOptions = new Headers({ 'Content-Type': 'application/json' });
    var requestOptions = new RequestOptions({ method: RequestMethod.Put, headers: headerOptions });
    return this.http.put('http://localhost:28750/api/Inventory/' + id,
      body,
      requestOptions).map(res => res.json());
  }
  getInventoryList(){
    this.http.get('http://localhost:28750/api/Inventory')
    .map((data : Response) =>{
      return data.json() as InventoryDetails[];
    }).toPromise().then(x => {
      this.inventoryList = x;
    })
  }

  deleteInventory(id: number) {
    return this.http.delete('http://localhost:28750/api/Inventory/' + id).map(res => res.json());
  }
}
