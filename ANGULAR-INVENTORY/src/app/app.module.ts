import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ToastrModule } from 'ngx-toastr';
import { InventoriesComponent } from './inventories/inventories.component';
import { InventoryComponent } from './inventories/inventory/inventory.component';
import { InventoryListComponent } from './inventories/inventory-list/inventory-list.component';
import { InventoryService } from "./inventories/shared/inventory.service"

@NgModule({
  declarations: [
    AppComponent,
    InventoriesComponent,
    InventoryComponent,
    InventoryListComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [InventoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
